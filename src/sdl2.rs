use bevy_app::prelude::Plugin;
use bevy_ecs::prelude::Event;
use bevy_ecs::prelude::Events;
use keycode::SDL_Keycode;
use scancode::SDL_Scancode;
use sdl2_sys::sdl::error::SDL_GetError;
use sdl2_sys::sdl::event::SDL_Event;
use sdl2_sys::sdl::event::SDL_PollEvent;
use sdl2_sys::sdl::event::SDL_KEYDOWN;
use sdl2_sys::sdl::event::SDL_KEYUP;
use sdl2_sys::sdl::event::SDL_MOUSEBUTTONDOWN;
use sdl2_sys::sdl::event::SDL_MOUSEBUTTONUP;
use sdl2_sys::sdl::event::SDL_MOUSEMOTION;
use sdl2_sys::sdl::event::SDL_MOUSEWHEEL;
use sdl2_sys::sdl::event::SDL_QUIT;
use sdl2_sys::sdl::event::SDL_WINDOWEVENT;
pub use sdl2_sys::sdl::keycode;
pub use sdl2_sys::sdl::mouse;
pub use sdl2_sys::sdl::scancode;
use sdl2_sys::sdl::syswm::SDL_GetWindowWMInfo;
use sdl2_sys::sdl::syswm::SDL_SysWMinfo;
use sdl2_sys::sdl::syswm::SDL_SYSWM_TYPE;
use sdl2_sys::sdl::video::SDL_CreateWindow;
use sdl2_sys::sdl::video::SDL_GetWindowSizeInPixels;
use sdl2_sys::sdl::video::SDL_SetWindowTitle;
use sdl2_sys::sdl::video::SDL_Window;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_CLOSE;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_ENTER;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_EXPOSED;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_FOCUS_GAINED;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_FOCUS_LOST;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_HIDDEN;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_HIT_TEST;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_LEAVE;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_MAXIMIZED;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_MINIMIZED;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_MOVED;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_NONE;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_RESIZED;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_RESTORED;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_SHOWN;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_SIZE_CHANGED;
use sdl2_sys::sdl::video::SDL_WINDOWEVENT_TAKE_FOCUS;
use sdl2_sys::sdl::video::SDL_WINDOW_ALLOW_HIGHDPI;
use sdl2_sys::sdl::video::SDL_WINDOW_RESIZABLE;
use sdl2_sys::sdl::video::SDL_WINDOW_SHOWN;
use sdl2_sys::sdl::SDL_Init;
use sdl2_sys::sdl::SDL_bool;
use sdl2_sys::sdl::SDL_INIT_VIDEO;
use tracing::error;

#[derive(Default)]
pub struct SDL2WindowPlugin;

impl Plugin for SDL2WindowPlugin {
    fn build(&self, app: &mut bevy_app::App) {
        app.insert_non_send_resource(Window::new("bevy_pgfx"))
            .set_runner(sdl_event_loop)
            .add_event::<SDLEvent>();
    }
}

pub struct Window {
    pub win: *mut SDL_Window,
    title: String,
    size: WindowSize,
    is_open: bool,
    has_focus: bool,
    is_cursor_inside: bool,
}

impl Window {
    pub fn new<S: AsRef<str>>(title: S) -> Self {
        assert!(unsafe { SDL_Init(SDL_INIT_VIDEO) } == 0);

        let c_title = std::ffi::CString::new(title.as_ref()).unwrap();
        let win = unsafe {
            SDL_CreateWindow(
                c_title.as_ptr(),
                0,
                0,
                1024,
                768,
                SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE,
            )
        };
        let size = WindowSize {
            width: 1024,
            height: 768,
            width_pixels: 1024,
            height_pixels: 768,
        };
        let is_open = true;

        Self {
            win,
            title: title.as_ref().to_string(),
            size,
            is_open,
            has_focus: false,
            is_cursor_inside: false,
        }
    }

    pub fn get_size(&self) -> WindowSize {
        self.size
    }

    pub fn is_open(&self) -> bool {
        self.is_open
    }

    pub fn has_focus(&self) -> bool {
        self.has_focus
    }

    pub fn is_cursor_inside(&self) -> bool {
        self.is_cursor_inside
    }

    pub fn get_title(&self) -> &str {
        &self.title
    }

    pub fn set_title<S: AsRef<str>>(&mut self, title: S) {
        self.title = title.as_ref().to_string();
        let c_title = std::ffi::CString::new(title.as_ref()).unwrap();
        unsafe {
            SDL_SetWindowTitle(self.win, c_title.as_ptr());
        }
    }

    pub fn get_platform_handle(&self) -> pgfx::common::PlatformHandle {
        let mut wmi: SDL_SysWMinfo = unsafe { std::mem::MaybeUninit::zeroed().assume_init() };
        wmi.version.major = 2;
        wmi.version.minor = 0;
        wmi.version.patch = 15;
        if unsafe { SDL_GetWindowWMInfo(self.win, &mut wmi) } == SDL_bool::False {
            let err_msg = unsafe { std::ffi::CStr::from_ptr(SDL_GetError() as _) };
            panic!(
                "[Window]: Failed to get window WMInfo: {}",
                err_msg.to_str().unwrap()
            );
        }
        unsafe {
            match wmi.subsystem {
                SDL_SYSWM_TYPE::Windows => pgfx::common::PlatformHandle::Win32 {
                    hwnd: wmi.info.win.window,
                    hdc: wmi.info.win.hdc,
                    hinstance: wmi.info.win.hinstance,
                },
                SDL_SYSWM_TYPE::X11 => pgfx::common::PlatformHandle::Xlib {
                    window: wmi.info.x11.window,
                    display: wmi.info.x11.display,
                },
                SDL_SYSWM_TYPE::Wayland => pgfx::common::PlatformHandle::Wayland {
                    surface: wmi.info.wl.surface,
                    display: wmi.info.wl.display,
                    egl_window: wmi.info.wl.egl_window,
                },
                _ => unimplemented!("{:?}", wmi.subsystem),
            }
        }
    }

    pub fn poll_event(&mut self) -> Option<SDLEvent> {
        let mut evt = SDL_Event::default();
        while unsafe { SDL_PollEvent(&mut evt as _) } != 0 {
            match unsafe { evt.ty } {
                SDL_QUIT => {
                    self.is_open = false;
                    return Some(SDLEvent::Quit);
                }
                SDL_WINDOWEVENT => match unsafe { evt.window.event } {
                    SDL_WINDOWEVENT_NONE => {
                        return Some(SDLEvent::Window(WindowEvent::None));
                    }
                    SDL_WINDOWEVENT_SHOWN => {
                        return Some(SDLEvent::Window(WindowEvent::Shown));
                    }
                    SDL_WINDOWEVENT_HIDDEN => {
                        return Some(SDLEvent::Window(WindowEvent::Hidden));
                    }
                    SDL_WINDOWEVENT_EXPOSED => {
                        return Some(SDLEvent::Window(WindowEvent::Exposed));
                    }
                    SDL_WINDOWEVENT_MOVED => unsafe {
                        return Some(SDLEvent::Window(WindowEvent::Moved(
                            evt.window.data1,
                            evt.window.data2,
                        )));
                    },
                    SDL_WINDOWEVENT_RESIZED => unsafe {
                        let mut w_pixels: i32 = 0;
                        let mut h_pixels: i32 = 0;
                        SDL_GetWindowSizeInPixels(self.win, &mut w_pixels, &mut h_pixels);
                        return Some(SDLEvent::Window(WindowEvent::Resized(
                            evt.window.data1,
                            evt.window.data2,
                            w_pixels,
                            h_pixels,
                        )));
                    },
                    SDL_WINDOWEVENT_SIZE_CHANGED => {
                        let mut w_pixels: i32 = 0;
                        let mut h_pixels: i32 = 0;
                        unsafe {
                            SDL_GetWindowSizeInPixels(self.win, &mut w_pixels, &mut h_pixels)
                        };
                        self.size = unsafe {
                            WindowSize {
                                width: evt.window.data1,
                                height: evt.window.data2,
                                width_pixels: w_pixels,
                                height_pixels: h_pixels,
                            }
                        };
                        return Some(SDLEvent::Window(WindowEvent::SizeChanged));
                    }
                    SDL_WINDOWEVENT_MINIMIZED => {
                        return Some(SDLEvent::Window(WindowEvent::Minimized));
                    }
                    SDL_WINDOWEVENT_MAXIMIZED => {
                        return Some(SDLEvent::Window(WindowEvent::Maximized));
                    }
                    SDL_WINDOWEVENT_RESTORED => {
                        return Some(SDLEvent::Window(WindowEvent::Restored));
                    }
                    SDL_WINDOWEVENT_ENTER => {
                        self.is_cursor_inside = true;
                        return Some(SDLEvent::Window(WindowEvent::Enter));
                    }
                    SDL_WINDOWEVENT_LEAVE => {
                        self.is_cursor_inside = false;
                        return Some(SDLEvent::Window(WindowEvent::Leave));
                    }
                    SDL_WINDOWEVENT_FOCUS_GAINED => {
                        self.has_focus = true;
                        return Some(SDLEvent::Window(WindowEvent::FocusGained));
                    }
                    SDL_WINDOWEVENT_FOCUS_LOST => {
                        self.has_focus = false;
                        return Some(SDLEvent::Window(WindowEvent::FocusLost));
                    }
                    SDL_WINDOWEVENT_CLOSE => {
                        self.is_open = false;
                        return Some(SDLEvent::Window(WindowEvent::Close));
                    }
                    SDL_WINDOWEVENT_TAKE_FOCUS => {
                        return Some(SDLEvent::Window(WindowEvent::TakeFocus));
                    }
                    SDL_WINDOWEVENT_HIT_TEST => {
                        return Some(SDLEvent::Window(WindowEvent::HitTest));
                    }
                    _ => {
                        error!("Unhandled window event: {:X}", unsafe { evt.ty });
                    }
                },
                SDL_KEYDOWN => {
                    let key_evt = unsafe {
                        KeyEvent {
                            state: State::Pressed,
                            scancode: evt.key.keysym.scancode,
                            key: evt.key.keysym.sym,
                            modifiers: evt.key.keysym.mod_,
                            repeat: evt.key.repeat,
                        }
                    };
                    return Some(SDLEvent::Key(key_evt));
                }
                SDL_KEYUP => {
                    let key_evt = unsafe {
                        KeyEvent {
                            state: State::Released,
                            scancode: evt.key.keysym.scancode,
                            key: evt.key.keysym.sym,
                            modifiers: evt.key.keysym.mod_,
                            repeat: evt.key.repeat,
                        }
                    };
                    return Some(SDLEvent::Key(key_evt));
                }
                SDL_MOUSEBUTTONDOWN => {
                    let mouse_btn_evt = unsafe {
                        MouseButtonEvent {
                            state: State::Pressed,
                            button: evt.button.button,
                            clicks: evt.button.clicks,
                            x: evt.button.x,
                            y: evt.button.y,
                        }
                    };
                    return Some(SDLEvent::MouseButton(mouse_btn_evt));
                }
                SDL_MOUSEBUTTONUP => {
                    let mouse_btn_evt = unsafe {
                        MouseButtonEvent {
                            state: State::Released,
                            button: evt.button.button,
                            clicks: evt.button.clicks,
                            x: evt.button.x,
                            y: evt.button.y,
                        }
                    };
                    return Some(SDLEvent::MouseButton(mouse_btn_evt));
                }
                SDL_MOUSEMOTION => {
                    let mouse_move_evt = unsafe {
                        MouseMoveEvent {
                            x: evt.motion.x,
                            y: evt.motion.y,
                            x_rel: evt.motion.xrel,
                            y_rel: evt.motion.yrel,
                            state: evt.motion.state,
                        }
                    };
                    return Some(SDLEvent::MouseMove(mouse_move_evt));
                }
                SDL_MOUSEWHEEL => unsafe {
                    return Some(SDLEvent::MouseWheel(MouseWheelEvent {
                        x: evt.wheel.x,
                        y: evt.wheel.y,
                        direction: evt.wheel.direction,
                    }));
                },
                _ => {}
            }
        }
        None
    }
}

fn sdl_event_loop(mut app: bevy_app::App) {
    let mut is_open = app
        .world
        .get_non_send_resource_mut::<Window>()
        .unwrap()
        .is_open;
    let mut evt = SDL_Event::default();
    while is_open {
        app.update();

        while unsafe { SDL_PollEvent(&mut evt as _) } != 0 {
            match unsafe { evt.ty } {
                SDL_QUIT => {
                    is_open = false;
                    app.world
                        .get_non_send_resource_mut::<Window>()
                        .unwrap()
                        .is_open = false;
                }
                SDL_WINDOWEVENT => match unsafe { evt.window.event } {
                    SDL_WINDOWEVENT_NONE => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::None));
                    }
                    SDL_WINDOWEVENT_SHOWN => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::Shown));
                    }
                    SDL_WINDOWEVENT_HIDDEN => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::Hidden));
                    }
                    SDL_WINDOWEVENT_EXPOSED => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::Exposed));
                    }
                    SDL_WINDOWEVENT_MOVED => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        unsafe {
                            evts.send(SDLEvent::Window(WindowEvent::Moved(
                                evt.window.data1,
                                evt.window.data2,
                            )));
                        }
                    }
                    SDL_WINDOWEVENT_RESIZED => {
                        let mut w_pixels: i32 = 0;
                        let mut h_pixels: i32 = 0;
                        unsafe {
                            SDL_GetWindowSizeInPixels(
                                app.world.get_non_send_resource_mut::<Window>().unwrap().win,
                                &mut w_pixels,
                                &mut h_pixels,
                            );
                        }
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        unsafe {
                            evts.send(SDLEvent::Window(WindowEvent::Resized(
                                evt.window.data1,
                                evt.window.data2,
                                w_pixels,
                                h_pixels,
                            )));
                        }
                    }
                    SDL_WINDOWEVENT_SIZE_CHANGED => {
                        let mut w_pixels: i32 = 0;
                        let mut h_pixels: i32 = 0;
                        unsafe {
                            SDL_GetWindowSizeInPixels(
                                app.world.get_non_send_resource_mut::<Window>().unwrap().win,
                                &mut w_pixels,
                                &mut h_pixels,
                            );
                        }
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::SizeChanged));
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .size = unsafe {
                            WindowSize {
                                width: evt.window.data1,
                                height: evt.window.data2,
                                width_pixels: w_pixels,
                                height_pixels: h_pixels,
                            }
                        };
                    }
                    SDL_WINDOWEVENT_MINIMIZED => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::Minimized));
                    }
                    SDL_WINDOWEVENT_MAXIMIZED => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::Maximized));
                    }
                    SDL_WINDOWEVENT_RESTORED => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::Restored));
                    }
                    SDL_WINDOWEVENT_ENTER => {
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .is_cursor_inside = true;

                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::Enter));
                    }
                    SDL_WINDOWEVENT_LEAVE => {
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .is_cursor_inside = false;
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::Leave));
                    }
                    SDL_WINDOWEVENT_FOCUS_GAINED => {
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .has_focus = true;
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::FocusGained));
                    }
                    SDL_WINDOWEVENT_FOCUS_LOST => {
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .has_focus = false;
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::FocusLost));
                    }
                    SDL_WINDOWEVENT_CLOSE => {
                        is_open = false;
                        app.world
                            .get_non_send_resource_mut::<Window>()
                            .unwrap()
                            .is_open = false;

                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::Close));
                    }
                    SDL_WINDOWEVENT_TAKE_FOCUS => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::TakeFocus));
                    }
                    SDL_WINDOWEVENT_HIT_TEST => {
                        let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                        evts.send(SDLEvent::Window(WindowEvent::HitTest));
                    }
                    _ => {
                        error!("Unhandled window event: {:X}", unsafe { evt.ty });
                    }
                },
                SDL_KEYDOWN => {
                    let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                    let key_evt = unsafe {
                        KeyEvent {
                            state: State::Pressed,
                            scancode: evt.key.keysym.scancode,
                            key: evt.key.keysym.sym,
                            modifiers: evt.key.keysym.mod_,
                            repeat: evt.key.repeat,
                        }
                    };
                    evts.send(SDLEvent::Key(key_evt));
                }
                SDL_KEYUP => {
                    let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                    let key_evt = unsafe {
                        KeyEvent {
                            state: State::Released,
                            scancode: evt.key.keysym.scancode,
                            key: evt.key.keysym.sym,
                            modifiers: evt.key.keysym.mod_,
                            repeat: evt.key.repeat,
                        }
                    };
                    evts.send(SDLEvent::Key(key_evt));
                }
                SDL_MOUSEBUTTONDOWN => {
                    let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                    let mouse_btn_evt = unsafe {
                        MouseButtonEvent {
                            state: State::Pressed,
                            button: evt.button.button,
                            clicks: evt.button.clicks,
                            x: evt.button.x,
                            y: evt.button.y,
                        }
                    };
                    evts.send(SDLEvent::MouseButton(mouse_btn_evt));
                }
                SDL_MOUSEBUTTONUP => {
                    let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                    let mouse_btn_evt = unsafe {
                        MouseButtonEvent {
                            state: State::Released,
                            button: evt.button.button,
                            clicks: evt.button.clicks,
                            x: evt.button.x,
                            y: evt.button.y,
                        }
                    };
                    evts.send(SDLEvent::MouseButton(mouse_btn_evt));
                }
                SDL_MOUSEMOTION => {
                    let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                    let mouse_move_evt = unsafe {
                        MouseMoveEvent {
                            x: evt.motion.x,
                            y: evt.motion.y,
                            x_rel: evt.motion.xrel,
                            y_rel: evt.motion.yrel,
                            state: evt.motion.state,
                        }
                    };
                    evts.send(SDLEvent::MouseMove(mouse_move_evt));
                }
                SDL_MOUSEWHEEL => {
                    let mut evts = app.world.get_resource_mut::<Events<SDLEvent>>().unwrap();
                    unsafe {
                        evts.send(SDLEvent::MouseWheel(MouseWheelEvent {
                            x: evt.wheel.x,
                            y: evt.wheel.y,
                            direction: evt.wheel.direction,
                        }));
                    }
                }
                _ => {}
            }
        }
    }
}

#[derive(Event, Debug, Copy, Clone)]
pub enum SDLEvent {
    Quit,
    Window(WindowEvent),
    Key(KeyEvent),
    MouseButton(MouseButtonEvent),
    MouseMove(MouseMoveEvent),
    MouseWheel(MouseWheelEvent),
}

#[derive(Event, Debug, Copy, Clone)]
pub enum WindowEvent {
    None,
    Shown,
    Hidden,
    Exposed,
    Moved(i32, i32),
    Resized(i32, i32, i32, i32), // width, height, width_pixels, height_pixels (for HIDPI, SDL
    // 2.26+)
    SizeChanged,
    Minimized,
    Maximized,
    Restored,
    Enter,       // mouse
    Leave,       // mouse
    FocusGained, // keyboard
    FocusLost,   // keyboard
    Close,
    TakeFocus,
    HitTest,
}

#[derive(Event, Copy, Clone, Debug)]
pub struct KeyEvent {
    pub state: State,
    pub scancode: SDL_Scancode,
    pub key: SDL_Keycode,
    pub modifiers: u16,
    pub repeat: u8,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum State {
    Released,
    Pressed,
}

#[derive(Event, Copy, Clone, Debug)]
pub struct MouseButtonEvent {
    pub state: State,
    pub button: u8,
    pub clicks: u8,
    pub x: i32,
    pub y: i32,
}

#[derive(Event, Copy, Clone, Debug)]
pub struct MouseMoveEvent {
    pub x: i32,
    pub y: i32,
    pub x_rel: i32,
    pub y_rel: i32,
    pub state: u32,
}

#[derive(Event, Copy, Clone, Debug)]
pub struct MouseWheelEvent {
    pub x: i32,
    pub y: i32,
    pub direction: u32,
}

#[derive(Default, Debug, Copy, Clone, PartialEq, Eq)]
pub struct WindowSize {
    pub width: i32,
    pub height: i32,
    pub width_pixels: i32,
    pub height_pixels: i32,
}
