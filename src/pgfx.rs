use bevy_app::{App, Plugin};
pub use pgfx::*;

#[derive(Default)]
pub struct PgfxPlugin;

impl Plugin for PgfxPlugin {
    fn build(&self, app: &mut App) {
        let win = app
            .world
            .get_non_send_resource_mut::<super::sdl2::Window>()
            .unwrap();
        let platform_handle = win.get_platform_handle();
        let gfx = Gfx::new(platform_handle).expect("Failed to create Gfx");
        app.insert_non_send_resource(gfx);
    }
}
