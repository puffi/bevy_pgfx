use bevy_app::prelude::Plugin;

pub mod pgfx;
pub mod sdl2;

pub struct BevyPgfxPlugin;

impl Plugin for BevyPgfxPlugin {
    fn build(&self, app: &mut bevy_app::App) {
        app.add_plugins(sdl2::SDL2WindowPlugin);
        app.add_plugins(pgfx::PgfxPlugin);
    }
}
